# 酱茄cms：WordPress知识付费小程序源码

#### Description
WordPress社区论坛+商城+资讯小程序 www.jiangqie.com


酱茄小程序开源版基于WordPress开源程序和WordPress REST API开发，实现WordPress网站数据与小程序数据同步共享，通过简单的配置就能搭建自己的小程序。

https://gitee.com/longwenjunj/jiangqie_kafei
